# Blink

This is an example repo of the blink program for the MSP-EXP430FR5994. It
includes a CI toolchain that compiles the program and a makefile to flash it.

It uses the TI supplied blink example.

## Setup

See `.gitlab-ci.yml` for the instructions on setting up the toolchain.

N.B. the `mspdebug` from the Debian repositories is too old to flash the MCU,
so please use the latest release from `dlbeer` (see CI).

## Makefile variables to set
The example is tailored to the MSP-EXP430FR5994 but it probably works on other
devices as well. The Makefile allows you to customise certain things:

- `DEVICE` should contain the device name, used to get the linker scripts and
  set the `-mmcu=` flag.
- `GCC_DIR` should point to the `msp430gcc` installation folder.
- `MSPDEBUG` should point to a `mspdebug` binary.

## Author

Mart Lubbers (mart@cs.ru.nl)
