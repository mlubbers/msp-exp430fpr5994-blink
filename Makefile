# Require DEVICE to be specified
ifndef DEVICE
$(info Please specify a device, e.g. DEVICE=MSP430F5529)
DEVICE=MSP430FR5994
DRIVERLIBDEVICE=MSP430FR5xx_6xx
endif

# Things to override by the user
GCC_DIR?=/opt/msp430gcc
MSPWARE_DIR?=/opt/msp430ware_3_80_14_01
MSPDEBUG?=mspdebug
CC:=msp430-elf-gcc

# Derived from the user defined variables
LDDIR:=$(GCC_MSP_INC_DIR)/$(shell echo $(DEVICE) | tr A-Z a-z)
DRIVER_DIR?=$(MSPWARE_DIR)/driverlib/driverlib/$(DRIVERLIBDEVICE)
GCC_MSP_INC_DIR?=$(GCC_DIR)/include
GCC_BIN_DIR?= $(GCC_DIR)/bin
GCC_INC_DIR?= $(GCC_DIR)/msp430-elf/include
PATH:=$(PATH):$(GCC_BIN_DIR)

CFLAGS+= -Wall -Wextra -std=c99 -Werror -Os -D__$(DEVICE)__ -mmcu=$(DEVICE) -g -ffunction-sections -fdata-sections
CFLAGS+=-I $(GCC_MSP_INC_DIR) -I $(GCC_INC_DIR) -I $(DRIVER_DIR)
LDFLAGS+=-T $(LDDIR).ld -L $(GCC_MSP_INC_DIR) -mmcu=$(DEVICE) -g -Wl,--gc-sections

all: main

main: main.o driverlib-$(DEVICE).a

driverlib-$(DEVICE).a: CFLAGS+=-Wno-error
driverlib-$(DEVICE).a: $(patsubst %.c,%.o,$(wildcard $(DRIVER_DIR)/*.c))
	ar rc $@ $^

flash: main
	msp430-elf-size $^
	$(MSPDEBUG) --force-reset ezfet "prog main"

clean:
	$(RM) main main.o driverlibe-$(DEVICE).a

purge: clean
	$(RM) driverlib-*.a
